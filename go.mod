module gitlab.com/marek2222/goTest

go 1.16

require (
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jung-kurt/gofpdf v1.16.2 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	gopkg.in/tomb.v2 v2.0.0-20161208151619-d5d1b5820637 // indirect
)
