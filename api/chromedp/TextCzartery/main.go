// Command text is a chromedp example demonstrating how to extract text from a
// specific element.
package main

import (
	"context"
	"log"
	"strings"

	"github.com/chromedp/chromedp"
)

func main() {
	// create context
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	// run task list
	var res string
	err := chromedp.Run(ctx,
		chromedp.Navigate(`https://biletyczarterowe.r.pl/formularz?klasa&oneWay=false&pakietIdPrzylot&pakietIdWylot=61287_206395&wiek%5B%5D=1991-08-05&wiek%5B%5D=2009-08-05&wiek%5B%5D=2020-08-05`),
		chromedp.Text(`#app div.destynacja section.section.accent-bg.bilety-section.bilety-section a.button.kupuje-button`,
			&res, chromedp.NodeVisible, chromedp.ByID),
	)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(strings.TrimSpace(res))
}
