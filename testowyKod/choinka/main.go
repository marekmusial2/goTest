package main

import (
	"fmt"
	"strings"
)

func main() {
	choinka(15)
}

func choinka(wys int) {
	for i := 0; i < wys; i++ {
		znak := "+"
		if i%2 == 0 {
			znak = "-"
		}
		if i%3 == 0 {
			znak = "*"
		}
		spacje := strings.Repeat(" ", wys-i)
		znaki := strings.Repeat(znak, 2*i+1)
		if i%2 == 0 && i%3 == 0 {
			znaki = ""
			for k := 0; k < (2*i + 1); k++ {
				if k%2 == 0 {
					znaki += "+"
				} else {
					znaki += "*"
				}
			}
		}
		linia := spacje + znaki + spacje
		fmt.Println(linia)
	}
}
