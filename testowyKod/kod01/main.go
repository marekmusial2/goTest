package main

import (
	"fmt"
	"reflect"
	"time"
)

func main() {
	zmienCzas()
	konwersjaNaFloat64(12)
	konwersjaNaFloat64(12.4)
	typIWartosc()
}

func typIWartosc() {
	var x interface{} = 12 //[]int{1, 2, 3}
	xType := reflect.TypeOf(x)
	xValue := reflect.ValueOf(x)
	fmt.Println("")
	fmt.Printf("typIWartosc: %v, %s", xType, xValue)
	// "[]int [1 2 3]"
}

func konwersjaNaFloat64(x interface{}) {
	//var x interface{}
	//x = int64(12.4)
	//x = 12
	var y float64
	switch v := x.(type) {
	case int:
		y = float64(v)
	case int8:
		y = float64(v)
	case int16:
		y = float64(v)
	case int32:
		y = float64(v)
	case int64:
		y = float64(v)
	case uint:
		y = float64(v)
	case uint8:
		y = float64(v)
	case uint16:
		y = float64(v)
	case uint32:
		y = float64(v)
	case uint64:
		y = float64(v)
	case float32:
		y = float64(v)
	case float64:
		y = v
	}
	fmt.Printf("y: %v, typ: %T\n", y, x)
}

func zmienCzas() {
	t := time.Now()
	fmt.Println(t)
	fmt.Println(t.AddDate(1, -5, 11))
}
