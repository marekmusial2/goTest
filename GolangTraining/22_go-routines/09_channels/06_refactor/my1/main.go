package main

import (
	"fmt"
)

func main() {
	c := przypisz()
	for n := range sumuj(c) {
		fmt.Println(n)
	}
}

func przypisz() chan int {
	wyn := make(chan int)
	go func() {
		for i := 0; i < 11; i++ {
			wyn <- i
		}
		close(wyn)
	}()
	return wyn
}

func sumuj(c chan int) chan int {
	wyn := make(chan int)
	go func() {
		var suma int
		for v := range c {
			suma += v
		}
		wyn <- suma
		close(wyn)
	}()
	return wyn
}
