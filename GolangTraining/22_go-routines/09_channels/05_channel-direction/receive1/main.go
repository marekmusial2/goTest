package main

import (
	"fmt"
)

func main() {
	// tylko odczyty
	a()
	b()
}

func a() {
	c := make(chan string, 1)
	c <- "hello"
	f(c)
}

func f(c <-chan string) {
	fmt.Println(<-c)
}

func b() {
	c := make(chan string, 1)
	c <- "coś"
	f2(c)
}

func f2(c <-chan string) {
	fmt.Println(<-c)
}
