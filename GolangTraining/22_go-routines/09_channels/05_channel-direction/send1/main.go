package main

import "fmt"

func main() {
	// only send channel
	a()
	b()
}

func a() {
	c := make(chan string, 1)
	a1(c)
	fmt.Println(<-c)
}

func a1(c chan<- string) {
	c <- "only send channel"
}

func b() {
	c := make(chan string, 1)
	a2(c)
	fmt.Println(<-c)
}

func a2(c chan<- string) {
	c <- "sent string to channel"
}
