package main

import "fmt"

func main() {
	// zlicz całkowitą liczbę goroutine
	// podaj wynik jako string: licznik np: Foo: 11
	c1 := przypisz("Foo:")
	c2 := przypisz("Bar:")
	c3 := sumuj(c1)
	c4 := sumuj(c2)
	fmt.Println("Licznik ogólny:", <-c3+<-c4)
}

func przypisz(s string) chan int {
	c := make(chan int)
	go func() {
		for i := 0; i < 16; i++ {
			c <- 1
			fmt.Println(s, i)
		}
		close(c)
	}()
	return c
}

func sumuj(c chan int) chan int {
	wyn := make(chan int)
	go func() {
		var suma int
		for v := range c {
			suma += v
		}
		wyn <- suma
		close(wyn)
	}()
	return wyn
}
