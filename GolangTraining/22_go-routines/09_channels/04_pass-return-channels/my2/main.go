package main

import "fmt"

func goroutine(ch chan bool) {
	fmt.Println("in goroutine") //1
	ch <- true
	//	close(ch)
}

func main() {
	ch := make(chan bool)
	go goroutine(ch)
	// for v := range ch {
	// 	fmt.Printf("ch: %v\n", v)
	// }
	<-ch
}
