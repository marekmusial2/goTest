package main

import "fmt"

func main() {
	c := przypisz()
	fmt.Println(c)
	cSum := sumuj(c)
	for v := range cSum {
		fmt.Println(v)
	}
}

func przypisz() chan int {
	k := make(chan int)
	go func() {
		for i := 0; i < 11; i++ {
			k <- i
		}
		close(k)
	}()
	return k
}

func sumuj(k chan int) chan int {
	wyn := make(chan int)
	go func() {
		var suma int
		for v := range k {
			suma += v
		}
		wyn <- suma
		close(wyn)
	}()
	return wyn
}
