package main

import (
	"fmt"

	winniepooh "gitlab.com/marek2222/goTest/GolangTraining/02_package/icomefromalaska"
	"gitlab.com/marek2222/goTest/GolangTraining/02_package/stringutil"
)

func main() {
	fmt.Println(stringutil.Reverse("!oG ,olleH"))
	fmt.Println(stringutil.MyName)
	fmt.Printf("BearName: %s", winniepooh.BearName)
	//fmt.Printf("BearName: %s", icomefromalaska.BearName)
}
