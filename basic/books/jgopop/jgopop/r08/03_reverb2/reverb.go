// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// Reverb2 jest serwerem TCP, który symuluje echo..
package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

/*
	Uruchom main'a jako serwer nasłuchujący.
	Uruchom 2-gi process(klienta) 03_netcat2.exe (go build) i wpisz Witaj:
	.\03_netcat2.exe
	Witaj
			WITAJ
	Juhu
			JUHU
			Witaj
			Juhu
			witaj
			juhu
	Działa współbieżnie, przy udziale go routine!
*/

func echo(c net.Conn, shout string, delay time.Duration) {
	fmt.Fprintln(c, "\t", strings.ToUpper(shout))
	time.Sleep(delay)
	fmt.Fprintln(c, "\t", shout)
	time.Sleep(delay)
	fmt.Fprintln(c, "\t", strings.ToLower(shout))
}

//!+
func handleConn(c net.Conn) {
	input := bufio.NewScanner(c)
	for input.Scan() {
		go echo(c, input.Text(), 2*time.Second)
	}
	// UWAGA: ignorowanie potencjalnych błędów z funkcji input.Err()
	c.Close()
}

//!-

func main() {
	l, err := net.Listen("tcp", "localhost:8000")
	if err != nil {
		log.Fatal(err)
	}
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Print(err) // np. przerwano połączenie
			continue
		}
		go handleConn(conn)
	}
}
