// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// Pipeline3 demonstruje skończony potok trzyetapowy
// z zakresem, zamknięciem oraz jednokierunkwymi typami kanałów.
package main

import "fmt"

func licznik(wynik chan<- int) {
	for i := 0; i < 21; i++ {
		wynik <- i
	}
	close(wynik)
}

//
func kwadrat(wynik chan<- int, dane <-chan int) {
	for v := range dane {
		wynik <- v * v
	}
	close(wynik)
}

func pokaz(dane <-chan int) {
	for v := range dane {
		fmt.Println(v)
	}
}

func main() {
	naturalneCh := make(chan int)
	kwadratoweCh := make(chan int)

	go licznik(naturalneCh)
	go kwadrat(kwadratoweCh, naturalneCh)
	pokaz(kwadratoweCh)
}

//!-
