// Golang program to illustrate
// reflect.SliceOf() Function

package main

import (
	"fmt"
	"reflect"
)

// Main function
func main() {
	example1()
	example2()
}

func example1() {
	// use of StructOf method
	typ := reflect.StructOf([]reflect.StructField{
		{
			Name: "Height",
			Type: reflect.TypeOf(float64(0)),
		},
		{
			Name: "Name",
			Type: reflect.TypeOf("abc"),
		},
	})

	fmt.Println(typ)

}

func example2() {

	// use of StructOf method
	tt := reflect.StructOf([]reflect.StructField{
		{
			Name: "Height",
			Type: reflect.TypeOf(0.0),
			Tag:  `json:"height"`,
		},
		{
			Name: "Name",
			Type: reflect.TypeOf("abc"),
			Tag:  `json:"name"`,
		},
	})

	fmt.Println(tt.NumField())
	fmt.Println(tt.Field(0))
	fmt.Println(tt.Field(1))
}
