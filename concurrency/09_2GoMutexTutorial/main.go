package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var Saldo float32 = 12000

type Klient struct {
	nazwa          string
	wplaconaKwota  float32
	wyplaconaKwota float32
}

func (u *Klient) wplata(wg *sync.WaitGroup, mx *sync.Mutex) {
	fmt.Printf("%s wpłaca %f zł \n", u.nazwa, u.wplaconaKwota)
	mx.Lock()
	Saldo += u.wplaconaKwota
	defer mx.Unlock()
	wg.Done()
}

func (u *Klient) wyplata(wg *sync.WaitGroup, mx *sync.Mutex) {
	fmt.Printf("%s wyplaca %f zł\n", u.nazwa, u.wyplaconaKwota)
	mx.Lock()
	Saldo -= u.wyplaconaKwota
	defer mx.Unlock()
	wg.Done()
}

func main() {

	var wg sync.WaitGroup
	var mu sync.Mutex
	klienci := []Klient{
		{nazwa: "Marco Lazerri", wyplaconaKwota: 1300, wplaconaKwota: 1000},
		{nazwa: "Paige Wilunda", wyplaconaKwota: 1400, wplaconaKwota: 123},
		{nazwa: "Gerry Riveres", wyplaconaKwota: 900, wplaconaKwota: 25},
		{nazwa: "Sean Bold", wyplaconaKwota: 200, wplaconaKwota: 5432},
		{nazwa: "Mike Wegner", wyplaconaKwota: 5600, wplaconaKwota: 2344},
	}
	rand.Seed(time.Now().UnixNano())
	for i := range klienci {
		wg.Add(2)
		i = rand.Intn(len(klienci))
		go klienci[i].wplata(&wg, &mu)
		go klienci[i].wyplata(&wg, &mu)
		time.Sleep(time.Second)

	}

	wg.Wait()

	fmt.Printf("Nowe saldo wynosi %f \n\n", Saldo)
}
