package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var Saldo float32 = 12000

type Klient struct {
	nazwa          string
	wplaconaKwota  float32
	wyplaconaKwota float32
}

func (u *Klient) wplata(wg *sync.WaitGroup) {
	fmt.Printf("%s wpłaca %f zł \n", u.nazwa, u.wplaconaKwota)
	Saldo += u.wplaconaKwota
	wg.Done()
}

func (u *Klient) wyplata(wg *sync.WaitGroup) {
	fmt.Printf("%s wyplaca %f zł\n", u.nazwa, u.wyplaconaKwota)
	Saldo -= u.wyplaconaKwota
	wg.Done()
}

func main() {

	var wg sync.WaitGroup
	klienci := []Klient{
		{nazwa: "Marco Lazerri", wyplaconaKwota: 1300, wplaconaKwota: 1000},
		{nazwa: "Paige Wilunda", wyplaconaKwota: 1400, wplaconaKwota: 123},
		{nazwa: "Gerry Riveres", wyplaconaKwota: 900, wplaconaKwota: 25},
		{nazwa: "Sean Bold", wyplaconaKwota: 200, wplaconaKwota: 5432},
		{nazwa: "Mike Wegner", wyplaconaKwota: 5600, wplaconaKwota: 2344},
	}
	rand.Seed(time.Now().UnixNano())
	for i := range klienci {
		wg.Add(2)
		i = rand.Intn(len(klienci))
		go klienci[i].wplata(&wg)
		go klienci[i].wyplata(&wg)
		time.Sleep(time.Second)

	}

	wg.Wait()

	fmt.Printf("Nowe saldo wynosi %f \n\n", Saldo)
}
