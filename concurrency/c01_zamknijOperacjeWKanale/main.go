package main

import (
	"fmt"
	"time"
)

// na podstawie Close operation on a channel in Go (Golang)
// https://golangbyexample.com/close-operation-on-a-channel-in-go-golang/
func main() {
	policzSume()
	// oczekiwano Sum: 6

}

func policzSume() {
	ch := make(chan int)
	go sum(ch, 3)
	ch <- 1
	ch <- 2
	ch <- 3
	close(ch)
	time.Sleep(time.Second * 1)
}

func sum(ch chan int, len int) {
	sum := 0
	for i := 0; i < len; i++ {
		sum += <-ch
	}
	fmt.Printf("Sum: %d\n", sum)
}
