package main

import (
	"fmt"
	"sync"
	"time"
)

func dosomething(millisecs time.Duration, wg *sync.WaitGroup) {
	duration := millisecs * time.Millisecond
	time.Sleep(duration)
	fmt.Println("Function in background, duration:", duration)
	wg.Done()
}

func version1() {
	fmt.Println("1")
	var wg sync.WaitGroup
	wg.Add(4)
	go dosomething(200, &wg)
	go dosomething(400, &wg)
	go dosomething(150, &wg)
	go dosomething(600, &wg)

	wg.Wait()
	fmt.Println("Done")
}

func version2() {
	fmt.Println("2")
	var wg sync.WaitGroup
	wg.Add(1)
	go dosomething(200, &wg)
	wg.Add(1)
	go dosomething(400, &wg)
	wg.Add(1)
	go dosomething(150, &wg)
	wg.Add(1)
	go dosomething(600, &wg)

	wg.Wait()
	fmt.Println("Done2")
}

func dosomething3(millisecs time.Duration, wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		duration := millisecs * time.Millisecond
		time.Sleep(duration)
		fmt.Println("Function in background, duration:", duration)
	}()
}

func version3() {
	fmt.Println("3")
	var wg sync.WaitGroup
	dosomething3(200, &wg)
	dosomething3(400, &wg)
	dosomething3(150, &wg)
	dosomething3(600, &wg)
	wg.Wait()
	fmt.Println("Done3")
}

func main() {
	version1()
	version2()
	version3()
}
