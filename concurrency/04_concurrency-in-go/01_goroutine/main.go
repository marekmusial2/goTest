package main

import (
	"fmt"
	"time"
)

func main() {
	_3()
	//_2()
	//_1()
}

func _3() {
	go count("sheep")
	go count("fish")
	// program nigdy się nie kończy
	fmt.Scanln()
}

func _2() {
	go count("sheep")
	go count("fish")
	// funkcja nie działa jeśli wszystkie wątki są odpalane równolegle
	// dlatego możemy dodać
	time.Sleep(time.Millisecond * 2)
}

func _1() {
	go count("sheep")
	count("fish")

	// program nigdy się nie kończy
	// gorutyna  jest odpalana równolegle
}

func count(thing string) {
	for i := 1; i <= 6; i++ {
		fmt.Println(i, thing)
		time.Sleep(time.Millisecond * 500)
	}
}
