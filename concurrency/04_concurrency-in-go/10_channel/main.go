package main

import "fmt"

// https://stackoverflow.com/questions/19621149/how-are-go-channels-implemented

var doneA = make(chan bool)
var doneB = make(chan bool)
var doneC = make(chan bool)

func main() {
	initB()
	initC()
}

func init() { // this runs when you program starts.
	go func() {
		doneA <- true //Give donA true
	}()
}

func initB() { //blocking
	go func() {
		a := <-doneA //will wait here until doneA is true
		// Do somthing here
		fmt.Print(a)
		doneB <- true //State you finished
	}()
}

func initC() {
	go func() {
		<-doneB // still blocking, but dont care about the value
		// some code here
		doneC <- true // Indicate finished this function
	}()
}
