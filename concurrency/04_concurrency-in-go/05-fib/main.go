package main

import "fmt"

func main() {
	counter := 50
	jobs := make(chan int, counter)
	results := make(chan int, counter)

	go worker(jobs, results)
	go worker(jobs, results)
	go worker(jobs, results)
	go worker(jobs, results)

	for i := 0; i < counter; i++ {
		jobs <- i
	}
	close(jobs)

	for j := 0; j < counter; j++ {
		fmt.Printf("%d: %d\n", j, <-results)
	}
}

// to kanały wysyłające i odbierające
func worker(jobs <-chan int, results chan<- int) {
	for n := range jobs {
		results <- fib(n)
	}
}

func fib(n int) int {
	if n <= 1 {
		return n
	}

	return fib(n-1) + fib(n-2)
}
