package main

import (
	"fmt"
	"time"
)

func main() {
	with_select()
	// with_2_channels()
	// ver_simple2()
	// ver_simple()
}

func with_select() {
	c1 := make(chan string)
	c2 := make(chan string)

	go func() {
		for {
			c1 <- "every 500 miliseconds"
			fmt.Println(time.Millisecond * 500)
		}
	}()

	go func() {
		for {
			c2 <- "every 2 seconds"
			fmt.Println(time.Second * 2)
		}
	}()

	for {
		select {
		case msg1 := <-c1:
			fmt.Println(msg1)
		case msg2 := <-c2:
			fmt.Println(msg2)
		}
	}
}

func with_2_channels() {
	c1 := make(chan string)
	c2 := make(chan string)

	go func() {
		for {
			c1 <- "every 500 miliseconds"
			time.Sleep(time.Millisecond * 500)
		}
	}()

	go func() {
		for {
			c2 <- "every 2 seconds"
			time.Sleep(time.Second * 2)
		}
	}()

	for {
		fmt.Println(<-c1)
		fmt.Println(<-c2)
	}
}

func ver_simple2() {
	c := make(chan string)
	c <- "hello"
	c <- "world"
	c <- "three"

	msg := <-c
	fmt.Println(msg)

	msg = <-c
	fmt.Println(msg)
}

func ver_simple() {
	c := make(chan string)
	c <- "hello"

	msg := <-c
	fmt.Println(msg)
}
