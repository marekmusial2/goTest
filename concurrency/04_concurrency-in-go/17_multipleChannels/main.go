// https://medium.com/@tilaklodha/concurrency-and-parallelism-in-golang-5333e9a4ba64

package main

import (
	"fmt"
)

func ParzysteLiczby(parzysta chan int) {
	i := 2
	for i < 9 {
		parzysta <- i
		i = i + 2
	}
	close(parzysta)
}

func NieparzysteLiczby(nieparzysta chan int) {
	i := 1
	for i < 9 {
		nieparzysta <- i
		i = i + 2
	}
	close(nieparzysta)
}

func main() {
	parzysta := make(chan int)
	nieparzysta := make(chan int)
	go ParzysteLiczby(parzysta)
	go NieparzysteLiczby(nieparzysta)
	for {
		parzysta, ok1 := <-parzysta
		nieparzysta, ok2 := <-nieparzysta
		if !ok1 && !ok2 {
			break
		}
		fmt.Println("Otrzymano ", parzysta, ok1, nieparzysta, ok2)
	}
}
