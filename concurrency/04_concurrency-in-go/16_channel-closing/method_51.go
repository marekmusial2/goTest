package main

import (
	"log"
	"math/rand"
	"sync"
	"time"
)

func close_gracefully_51_mO_1N() {
	//rand.Seed(time.Now().UnixNano()) // needed before Go 1.20
	rand := rand.New(rand.NewSource(time.Now().UnixNano()))
	log.SetFlags(0)

	// ...
	const Max = 30
	const NumReceivers = 10

	wgReceivers := sync.WaitGroup{}
	wgReceivers.Add(NumReceivers)

	// ...
	dataCh := make(chan int)

	// the sender
	go func() {
		for {
			time.Sleep(time.Millisecond * 5)
			if value := rand.Intn(Max); value == 0 {
				// The only sender can close the
				// channel at any time safely.
				close(dataCh)
				return
			} else {
				dataCh <- value
			}
		}
	}()

	// receivers
	for i := 0; i < NumReceivers; i++ {
		go func() {
			defer wgReceivers.Done()

			// Receive values until dataCh is
			// closed and the value buffer queue
			// of dataCh becomes empty.
			for value := range dataCh {
				log.Println(value)
			}
		}()
		log.Println("Licznik:", i)
	}

	wgReceivers.Wait()
}
