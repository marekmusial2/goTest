package main

import (
	"fmt"
	"sync"
)

type MyChannel struct {
	C    chan T
	once sync.Once
}

func NewMyChannel() *MyChannel {
	return &MyChannel{C: make(chan T)}
}

func (mc *MyChannel) SafeClose() {
	mc.once.Do(func() {
		close(mc.C)
	})
}

func close_channels_politely() {
	c := NewMyChannel()
	fmt.Println(IsClosed(c.C)) // false
	SafeClose(c.C)
	fmt.Println(IsClosed(c.C)) // true

	SafeSend(c.C, 2)
	fmt.Println(IsClosed(c.C)) // true
}
