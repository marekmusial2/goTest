package main

import (
	"fmt"
	"sync"
)

type MyChannelMx struct {
	C      chan T
	closed bool
	mutex  sync.Mutex
}

func NewMyChannelMx() *MyChannelMx {
	return &MyChannelMx{C: make(chan T)}
}

func (mc *MyChannelMx) SafeCloseMx() {
	mc.mutex.Lock()
	defer mc.mutex.Unlock()
	if !mc.closed {
		close(mc.C)
		mc.closed = true
	}
}

func (mc *MyChannelMx) IsClosed() bool {
	mc.mutex.Lock()
	defer mc.mutex.Unlock()
	return mc.closed
}

func close_politely_by_mutex() {
	c := NewMyChannelMx()
	fmt.Println(c.IsClosed()) // false
	SafeClose(c.C)
	fmt.Println(c.IsClosed()) // true

	SafeSend(c.C, 2)
	fmt.Println(IsClosed(c.C)) // true
}
