package main

import (
	"fmt"
)

func close_channels_rudely() {
	c := make(chan T)
	fmt.Println(IsClosed(c)) // false
	SafeClose(c)
	fmt.Println(IsClosed(c)) // true

	SafeSend(c, 2)
	fmt.Println(IsClosed(c)) // true
}

func SafeSend(ch chan T, value T) (closed bool) {
	defer func() {
		if recover() != nil {
			closed = true
		}
	}()

	ch <- value  // panic if ch is closed
	return false // <=> closed = false; return
}

func SafeClose(ch chan T) (justClosed bool) {
	defer func() {
		if recover() != nil {
			// The return result can be altered
			// in a defer function call.
			justClosed = false
		}
	}()

	// assume ch != nil here.
	close(ch)   // panic if ch is closed
	return true // <=> justClosed = true; return
}
