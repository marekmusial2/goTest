package main

import (
	"log"
	"sync"
	"time"
)

func close_gracefully_61_mO_1N() {
	//rand := rand.New(rand.NewSource(time.Now().UnixNano()))
	log.SetFlags(0)

	// ...
	const Max = 30
	const NumReceivers = 10

	wgReceivers := sync.WaitGroup{}
	wgReceivers.Add(NumReceivers)

	// ...
	dataCh := make(chan int)

	// the sender
	go func() {
		counter := 0
		for {
			counter++
			time.Sleep(time.Millisecond * 100)
			if value := counter; counter == Max {
				// The only sender can close the
				// channel at any time safely.
				close(dataCh)
				return
			} else {
				dataCh <- value
			}
		}
	}()

	// receivers
	for i := 0; i < NumReceivers; i++ {
		go func() {
			defer wgReceivers.Done()

			// Receive values until dataCh is
			// closed and the value buffer queue
			// of dataCh becomes empty.
			for value := range dataCh {
				log.Println(value)
			}
		}()
		log.Println("Odbiornik:", i+1)
	}

	wgReceivers.Wait()
}
