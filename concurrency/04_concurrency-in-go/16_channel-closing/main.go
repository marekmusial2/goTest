package main

// How to Gracefully Close Channels
// https://go101.org/article/channel-closing.html

func main() {
	choice := 63
	if choice == 1 {
		simple_method()
	} else if choice == 2 {
		close_channels_rudely()
	} else if choice == 3 {
		close_channels_politely()
	} else if choice == 4 {
		close_politely_by_mutex()
		// zamykają kanały z wdziękiem
	} else if choice == 51 {
		// 1. M odbiorców, jeden nadawca,
		// nadawca mówi „nigdy więcej nie wysyłam”,
		// zamykając kanał danych
		close_gracefully_51_mO_1N()
	} else if choice == 52 {
		// 2. Jeden odbiornik, N nadawców,
		// jedyny odbiornik mówi „przestań wysyłać więcej”,
		// zamykając dodatkowy kanał sygnałowy
		close_gracefully_52_1O_nN()
	} else if choice == 53 {
		// 3. M odbiorników , N nadawców,
		// każdy z nich mówi „zakończmy grę”,
		// powiadamiając moderatora o zamknięciu
		// dodatkowego kanału sygnałowego
		close_gracefully_53_mO_nN()
	} else if choice == 54 {
		// 4. Wariant sytuacji „M odbiorników, jeden nadawca”:
		// wniosek o zamknięcie składa goroutine strony trzeciej
		// Czasami potrzebne jest, aby goroutine
		// innej firmy musiał wykonać ścisły sygnał.
		// W takich przypadkach możemy użyć
		// dodatkowego kanału sygnałowego,
		// aby powiadomić nadawcę o zamknięciu kanału danych.
		close_gracefully_54_mO_1N()
	} else if choice == 55 {
		// 5. Wariant sytuacji „N nadawcy”:
		// kanał danych musi być zamknięty,
		// aby poinformować odbiorniki, że wysyłanie danych się skończyło
		close_gracefully_55_mO_nN()
	} else if choice == 61 {
		// 1. M odbiorców, jeden nadawca,
		// nadawca mówi „nigdy więcej nie wysyłam”,
		// zamykając kanał danych
		close_gracefully_61_mO_1N()
	} else if choice == 63 {
		// 3. M odbiorników , N nadawców,
		// każdy z nich mówi „zakończmy grę”,
		// powiadamiając moderatora o zamknięciu
		// dodatkowego kanału sygnałowego
		close_gracefully_63_mO_nN()
	}
}
