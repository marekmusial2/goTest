package main

// Atomic Operations in Golang – atomic package
// By Sutirtha Chakraborty / March 16, 2020
// https://golangdocs.com/atomic-operations-in-golang-atomic-package

/*
Operacje atomowe są używane, gdy potrzebujemy wspólnej zmiennej
między różnymi goroutinami, która zostanie przez nich zaktualizowana.
Jeśli operacja aktualizacji nie zostanie zsynchronizowana,
spowoduje to problem, który widzieliśmy.

Operacje atomowe rozwiązują ten problem, synchronizując dostęp
do współdzielonej zmiennej i poprawiając dane wyjściowe.
*/

func main() {
	choice := 2
	if choice == 1 {
		atomic_no_sync()
	} else if choice == 2 {
		atomic_with_sync()
	}
}
