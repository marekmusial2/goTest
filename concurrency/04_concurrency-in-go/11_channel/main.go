// https://stackoverflow.com/questions/39213230/how-to-test-if-a-channel-is-close-and-only-send-to-it-when-its-not-closed
package main

import (
	"fmt"
)

func main() {

	mychan := make(chan int)
	donechannel := make(chan struct{})
	go pushchannel(mychan)
	go drainchan(mychan, donechannel)
	_, ok := <-donechannel
	if !ok {
		fmt.Println("can not read from donechannel this means donechannel is closed, means we are done :)")
	}
	fmt.Println("Done")
}

func pushchannel(ch chan int) {
	fmt.Println("pushing to chan")
	for i := 0; i <= 10; i++ {
		fmt.Printf("pushed %v\n", i)
		ch <- i
	}
	close(ch)
}

func drainchan(ch chan int, donechannel chan struct{}) {
	fmt.Println("draining")
	for {
		res, ok := <-ch
		if !ok {
			fmt.Println("result channel is closed, we can signal the donechannel now.")
			close(donechannel)
			break
		} else {
			fmt.Printf("got result %v\n", res)
		}
	}
}
