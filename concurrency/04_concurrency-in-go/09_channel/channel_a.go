package main

import (
	"fmt"
	"math/rand"
	"time"
)

func channel_a() {
	ch := longTimedOperation()
	fmt.Println(ch)
	ch2 := longTimedOperation()
	fmt.Println(ch2)
}

func longTimedOperation() <-chan int32 {
	ch := make(chan int32)
	go run_a(ch)
	return ch
}

func run_a(ch chan int32) {
	defer close(ch)
	time.Sleep(time.Second * 5)
	ch <- rand.Int31n(300)
}
