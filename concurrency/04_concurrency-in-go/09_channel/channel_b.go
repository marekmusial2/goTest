package main

import (
	"fmt"
	"time"
)

type T = struct{}

func channel_b() {
	completed := make(chan T)
	go run_b(completed)
	completed <- struct{}{} // blocked waiting for a notification
	go run_b(completed)
	completed <- struct{}{} // blocked waiting for a notification
	fmt.Println("pong")
}

func run_b(completed chan T) {
	fmt.Println("ping")
	time.Sleep(time.Second * 2) // heavy process simulation
	<-completed                 // receive a value from completed channel
}
