package main

import (
	"fmt"
	"sync"
)

func wg_annonymous_function() {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		fmt.Println("Running anonymous function")
		wg.Done()
	}()
	wg.Wait()
	fmt.Println("Done executing")
}
