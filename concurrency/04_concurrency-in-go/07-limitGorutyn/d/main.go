package main

import (
	"fmt"
	"sync"
)

var jobQ chan int
var wg sync.WaitGroup

func main() {
	jobQ = make(chan int, 100)

	go func() {
		wg.Add(1)
		defer wg.Done()
		//Spawn 10 workers
		for i := 0; i < 10; i++ {
			fmt.Println("Spawn :", i)
			wg.Add(1)
			go worker(jobQ)
		}
	}()

	for i := 0; i < 1000; i++ {
		jobQ <- i
	}
	close(jobQ)

	wg.Wait()
}

func worker(jobs chan int) {
	defer wg.Done()
	for job := range jobs {
		fmt.Println(job)
	}
}
