package main

import (
	"fmt"
	"sync"
)

func main() {
	const max = 8
	queue := make(chan int, max)
	sumCh := make(chan int, max)

	wg := &sync.WaitGroup{}
	for i := 0; i < max; i++ {
		wg.Add(2)
		go worker(wg, queue)
		go countSum(wg, queue, sumCh)
	}
	for i := 0; i < 25; i++ {
		queue <- i
	}
	for v := range sumCh {
		fmt.Println("Suma: ", v)
	}

	close(queue)
	close(sumCh)
	wg.Wait()
	fmt.Println("Done")
}

func worker(wg *sync.WaitGroup, queue chan int) {
	defer wg.Done()
	for job := range queue {
		fmt.Println(job, " ") // a job
	}
}

func countSum(wg *sync.WaitGroup, queue chan int, sumCh chan int) {
	defer wg.Done()
	sum := 0
	for job := range queue {
		sum += job
	}
	sumCh <- sum
}
