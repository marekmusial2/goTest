package main

import (
	"fmt"
	"sync"
)

func main() {
	const max = 8
	queue := make(chan int, max)
	wg := &sync.WaitGroup{}
	for i := 0; i < max; i++ {
		wg.Add(1)
		go worker(wg, queue)
	}
	for i := 0; i < 25; i++ {
		queue <- i
	}
	close(queue)
	wg.Wait()
	fmt.Println("Done")
}
func worker(wg *sync.WaitGroup, queue chan int) {
	defer wg.Done()
	for job := range queue {
		fmt.Println(job, " ") // a job
	}
}
