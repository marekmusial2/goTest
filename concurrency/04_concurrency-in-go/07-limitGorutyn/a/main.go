package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	var wg = sync.WaitGroup{}
	maxGoroutines := 10
	guard := make(chan struct{}, maxGoroutines)

	for i := 0; i < 30; i++ {
		guard <- struct{}{}
		wg.Add(1)
		go func(n int) {
			worker(n)
			<-guard
			wg.Done()
		}(i)
	}

	wg.Wait()
}

func worker(i int) {
	fmt.Println("doing work on", i)
	time.Sleep(time.Millisecond * 500)
}
