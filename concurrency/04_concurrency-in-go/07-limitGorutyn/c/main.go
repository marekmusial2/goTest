package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

// https://stackoverflow.com/questions/25306073/always-have-x-number-of-goroutines-running-at-any-time

type idProcessor func(id uint)

func DoSquare(limit uint, proc idProcessor) chan<- uint {
	ch := make(chan uint)
	for i := uint(0); i < limit; i++ {
		go func() {
			for {
				id, ok := <-ch
				if !ok {
					return
				}
				proc(id)
			}
		}()
	}
	return ch
}

func main() {
	runtime.GOMAXPROCS(4)
	var wg sync.WaitGroup //this is just for the demo, otherwise main will return
	fn := func(id uint) {
		// Pokaż licznik i kwadrat licznika
		fmt.Printf("%d: %d\n", id, id*id)
		time.Sleep(time.Millisecond * 800)
		wg.Done()
	}
	wg.Add(30)
	ch := DoSquare(10, fn)
	for i := uint(0); i < 30; i++ {
		ch <- i
	}
	close(ch) //should do this to make all the goroutines exit gracefully
	wg.Wait()
}
