package main

import (
	"fmt"
	"time"
)

func main() {
	wybor := 1
	if wybor == 1 {
		wszystkie_krotszy_zapis()
	} else if wybor == 2 {
		wszystkie_watrosci_z_zamknieciem()
	} else if wybor == 3 {
		wszystkie_watrosci_z_deadlockiem()
	} else if wybor == 4 {
		ostatnia_wartosc_z_kanalu()
	}

}

func wszystkie_krotszy_zapis() {
	c := make(chan string)
	go licznik_z_zamknieciem("sheep", c)
	//go licznik_z_zamknieciem("sheep", c)

	for msg := range c {
		fmt.Println(msg)
	}
}

func wszystkie_watrosci_z_zamknieciem() {
	c := make(chan string, 10)
	go licznik_z_zamknieciem("sheep", c)

	for {
		msg, open := <-c
		if !open {
			break
		}
		fmt.Println(msg)
	}
}

func licznik_z_zamknieciem(thing string, c chan string) {
	for i := 1; i <= 5; i++ {
		c <- thing
		time.Sleep(time.Millisecond * 500)
	}
	close(c)
}

func wszystkie_watrosci_z_deadlockiem() {
	c := make(chan string)
	go count3("sheep", c)

	for {
		msg := <-c
		fmt.Println(msg)
	}
}

func ostatnia_wartosc_z_kanalu() {
	c := make(chan string)
	go count3("sheep", c)

	msg := <-c
	fmt.Println(msg)
}

func count3(thing string, c chan string) {
	for i := 1; i <= 5; i++ {
		c <- thing
		time.Sleep(time.Millisecond * 500)
	}
}
