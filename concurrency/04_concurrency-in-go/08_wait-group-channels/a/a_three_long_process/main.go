// https://dev.to/sophiedebenedetto/synchronizing-go-routines-with-channels-and-waitgroups-3ke2

package main

import (
	"fmt"
	"sync"
)

func main() {
	// Create a WaitGroup to manage the goroutines.
	var waitGroup sync.WaitGroup
	c := make(chan string)

	// Perform 3 concurrent transactions against the database.
	waitGroup.Add(3)

	go func() {
		waitGroup.Wait()
		close(c)
	}()

	go func() {
		defer waitGroup.Done()

		c <- "one"
	}()

	go func() {
		defer waitGroup.Done()

		c <- "two"
	}()

	go func() {
		defer waitGroup.Done()

		c <- "three"
	}()

	for str := range c {
		fmt.Println(str)
	}
}
