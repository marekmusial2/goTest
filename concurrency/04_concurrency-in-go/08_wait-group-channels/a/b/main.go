package main

import (
	"fmt"
)

func main() {
	s := []int{4, 2, 8, -7, 4, 0}

	chSuma := make(chan int)
	go sum(s[:len(s)/2], chSuma)
	go sum(s[len(s)/2:], chSuma)

	x, y := <-chSuma, <-chSuma
	fmt.Println(x, y)
}

func sum(tabl []int, c chan int) {
	sum := 0
	for _, value := range tabl {
		sum += value
	}
	c <- sum
}
