package main

import (
	"fmt"
	"sync"
)

func doSomething(wg *sync.WaitGroup) {
	fmt.Println("done something")
	defer wg.Done()
}
func doOther(wg *sync.WaitGroup) {
	fmt.Println("done other")
	defer wg.Done()
}

func main() {
	var wg sync.WaitGroup
	wg.Add(1)
	go doSomething(&wg)
	wg.Add(1)
	go doOther(&wg)

	defer wg.Wait()

	// program will wait uti doSomething and doOther complete
	done := make(chan bool)
	go func() {
		for {
			select {
			case <-done:
				return
			default:
			}
		}
	}()
	done <- true
	fmt.Println("Done channel")
}
