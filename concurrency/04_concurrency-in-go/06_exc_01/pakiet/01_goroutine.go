package pakiet

import "fmt"

func Goroutine_suma() {
	go sumaLiczb(4)
	go sumaLiczb(5)
	go sumaLiczb(6)
	fmt.Scanln()
}

func sumaLiczb(maksWart int) {
	suma := 0
	for i := 0; i < maksWart; i++ {
		suma += i + 1
	}
	fmt.Printf("suma liczb 1 do %d = %d\n", maksWart, suma)
}
