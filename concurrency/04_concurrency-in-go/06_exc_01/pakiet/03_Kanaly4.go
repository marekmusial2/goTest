package pakiet

import (
	"fmt"
)

func Kanaly_4() {
	ch := make(chan int)
	go producer4(ch)
	for v := range ch {
		fmt.Println("Received ", v)
	}
}

func producer4(chnl chan int) {
	for i := 0; i < 7; i++ {
		chnl <- i
	}
	close(chnl)
}
