package pakiet

import (
	"fmt"
)

func Kanaly_2() {
	c1 := make(chan int)
	c2 := make(chan int)

	go sumaLiczb6(4, c1)
	go sumaLiczb6(5, c2)

	suma1, suma2 := <-c1, <-c2
	fmt.Println("suma1:", suma1)
	fmt.Println("suma2:", suma2)
	fmt.Println("Finalnie", suma1+suma2)
}

func sumaLiczb6(maksWart int, c chan int) {
	suma := 0
	for i := 0; i < maksWart; i++ {
		suma += i + 1
	}
	c <- suma
	close(c)
}
