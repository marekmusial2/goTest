package pakiet

import (
	"fmt"
)

func Kanaly_1() {
	c := make(chan int)

	go sumaLiczb5(4, c)
	go sumaLiczb5(5, c)
	//go sumaLiczb5(6, c)

	for suma := range c {
		fmt.Println(suma)
	}
}

func sumaLiczb5(maksWart int, c chan int) {
	suma := 0
	for i := 0; i < maksWart; i++ {
		suma += i + 1
	}
	c <- suma
	close(c)
}
