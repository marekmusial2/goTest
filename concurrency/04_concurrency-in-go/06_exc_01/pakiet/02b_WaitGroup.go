package pakiet

import (
	"fmt"
	"sync"
)

func WaitGroup_Zapis() {
	var wg sync.WaitGroup
	wg.Add(3)

	go uruchom(&wg, 4)
	go uruchom(&wg, 5)
	go uruchom(&wg, 6)

	wg.Wait()
}

func uruchom(wg *sync.WaitGroup, liczba int) {
	defer wg.Done()
	sumaLiczb3(liczba)
}

func sumaLiczb3(maksWart int) {
	suma := 0
	for i := 0; i < maksWart; i++ {
		suma += i + 1
	}
	fmt.Printf("suma liczb 1 do %d = %d\n", maksWart, suma)
}
