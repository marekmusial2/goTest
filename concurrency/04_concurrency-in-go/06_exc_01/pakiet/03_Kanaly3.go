package pakiet

import (
	"fmt"
)

func Kanaly_3() {
	ch := make(chan int)
	go producer(ch)
	for {
		v, ok := <-ch
		if !ok {
			break
		}
		fmt.Println("Received ", v, ok)
	}
}

func producer(chnl chan int) {
	for i := 0; i < 8; i++ {
		chnl <- i
	}
	close(chnl)
}
