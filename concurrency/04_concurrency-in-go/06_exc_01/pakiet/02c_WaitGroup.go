package pakiet

import (
	"fmt"
	"sync"
)

func WaitGroupC_InnyZapis() {
	wg := new(sync.WaitGroup)
	wg.Add(3)

	go uruchom4(wg, 4)
	go uruchom4(wg, 5)
	go uruchom4(wg, 6)

	wg.Wait()
}

func uruchom4(wg *sync.WaitGroup, liczba int) {
	defer wg.Done()
	sumaLiczb4(liczba)
}

func sumaLiczb4(maksWart int) {
	suma := 0
	for i := 0; i < maksWart; i++ {
		suma += i + 1
	}
	fmt.Printf("suma liczb 1 do %d = %d\n", maksWart, suma)
}
