package pakiet

import (
	"fmt"
	"sync"
)

func WaitGroup_suma() {
	var wg sync.WaitGroup
	wg.Add(3)

	go func() {
		defer wg.Done()
		sumaLiczb2(4)
	}()

	go func() {
		defer wg.Done()
		sumaLiczb2(5)
	}()

	go func() {
		defer wg.Done()
		sumaLiczb2(6)
	}()

	wg.Wait()
}

func sumaLiczb2(maksWart int) {
	suma := 0
	for i := 0; i < maksWart; i++ {
		suma += i + 1
	}
	fmt.Printf("suma liczb 1 do %d = %d\n", maksWart, suma)
}
