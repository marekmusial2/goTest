package main

import (
	"fmt"
	"sync"
	"time"
)

var (
	licznik int
	rwLock  sync.RWMutex
	mx      sync.RWMutex
)

func increment() {
	mx.Lock()
	licznik++
	mx.Unlock()
}

func odczytaj(wg *sync.WaitGroup) {
	rwLock.RLock()
	defer rwLock.RUnlock()

	fmt.Println("Odczyt zablokowany...")
	time.Sleep(time.Second)
	fmt.Println("Odczyt odblokowany...")
	wg.Done()
}
func zapisz(wg *sync.WaitGroup) {
	rwLock.Lock()
	defer rwLock.Unlock()

	fmt.Println("Zapis zablokowany...")
	time.Sleep(time.Second)
	fmt.Println("Zapis odblokowany...")
	wg.Done()
}

func odczytywaczZapisywacz(wg *sync.WaitGroup) {
	wg.Add(5)
	go zapisz(wg)
	go odczytaj(wg)
	go odczytaj(wg)
	go odczytaj(wg)
	go zapisz(wg)
	time.Sleep(time.Second)
	fmt.Println("Wykonane ...")
	wg.Done()
}

func main() {
	var wg sync.WaitGroup
	odczytywaczZapisywacz(&wg)
	wg.Wait()
	fmt.Println("Główny wątek się skończył")
}
