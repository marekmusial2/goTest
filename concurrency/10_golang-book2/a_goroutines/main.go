package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	//show := "a"
	//show := "b"
	show := "c" // with delay

	switch show {
	case "a":
		goroutine_a()
	case "b":
		goroutine_b()
	case "c":
		goroutine_c()
	}
	var input string
	fmt.Scanln(&input)
}

// a and b
func f(n int) {
	for i := 0; i < 5; i++ {
		fmt.Println(n, ":", i)
	}
}

func goroutine_a() {
	go f(1)
}

func goroutine_b() {
	for i := 0; i < 3; i++ {
		go f(i)
	}
}

// c
func f3(n int) {
	for i := 0; i < 5; i++ {
		fmt.Println(n, ":", i)
		amt := time.Duration(rand.Intn(1050))
		time.Sleep(time.Millisecond * amt)
	}
}
func goroutine_c() {
	for i := 0; i < 3; i++ {
		go f3(i)
	}
}
