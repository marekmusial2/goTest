package main

import (
	"fmt"
	"time"
)

func main() {
	// a -
	// b - with timeout
	show := "b"

	switch show {
	case "a":
		select_a()
	case "b":
		select_b()
	}
	var input string
	fmt.Scanln(&input)
}

func select_a() {
	c2 := make(chan string)
	c3 := make(chan string)

	go func() {
		for {
			c2 <- "from 2"
			time.Sleep(time.Second * 2)
		}
	}()

	go func() {
		for {
			c3 <- "from 3"
			time.Sleep(time.Second * 3)
		}
	}()

	go func() {
		for {
			select {
			case msg1 := <-c2:
				fmt.Println(msg1)
			case msg2 := <-c3:
				fmt.Println(msg2)
			}
		}
	}()
}

func select_b() {
	c1 := make(chan string)
	c2 := make(chan string)

	go func() {
		for {
			c1 <- "from 1"
			time.Sleep(time.Second * 2)
		}
	}()

	go func() {
		for {
			c2 <- "from 2"
			time.Sleep(time.Second * 3)
		}
	}()

	select {
	case msg1 := <-c1:
		fmt.Println("Message 1", msg1)
	case msg2 := <-c2:
		fmt.Println("Message 2", msg2)
	case <-time.After(time.Second):
		fmt.Println("timeout")
	default:
		fmt.Println("nothing ready")
	}
}
