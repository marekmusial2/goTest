package main

import (
	"fmt"
	"time"
)

func main() {
	// a - forever ping
	// b - forever ping and pong
	show := "b"

	switch show {
	case "a":
		channel_a()
	case "b":
		channel_b()
	}
	var input string
	fmt.Scanln(&input)
}

// a
func pinger(c chan string) {
	for {
		c <- "ping"
	}
}
func ponger(c chan string) {
	for {
		c <- "pong"
	}
}

func printer(c chan string) {
	for {
		msg := <-c
		fmt.Println(msg)
		time.Sleep(time.Second * 1)
	}
}

func channel_a() {
	var c chan string = make(chan string)

	go pinger(c)
	go printer(c)
}

func channel_b() {
	var c chan string = make(chan string)

	go pinger(c)
	go ponger(c)
	go printer(c)
}
