package main

import (
	"fmt"
	"sync"
)

func f1(v *int, wg *sync.WaitGroup, m *sync.Mutex) {
	// acquire lock
	m.Lock()
	// do operation
	*v++
	// release lock
	m.Unlock()
	wg.Done()
}

func solution_1() {

	var wg sync.WaitGroup
	// declare mutex
	var m sync.Mutex
	var v int = 0

	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go f1(&v, &wg, &m)
	}

	wg.Wait()
	fmt.Println("Finished", v)
}
