package main

import (
	"fmt"
	"sync"
	"time"
)

// Ilustracja problemu śpiącego fryzjera
// Fryzjer ma jedno krzesło fryzjerskie w krojowni
// i poczekalnię z kilkoma krzesłami. Kiedy fryzjer
// kończy obcinanie włosów klienta, zwalnia klienta i idzie do poczekalni,
// aby sprawdzić, czy czekają inni. Jeśli tak, przynosi jednego z nich
// z powrotem na krzesło i obcina im włosy. Jeśli nie ma, wraca
// na krzesło i śpi w nim. Każdy klient po przyjeździe wygląda,
// aby zobaczyć, co robi fryzjer. Jeśli fryzjer śpi, klient budzi go
// i siedzi na krześle krojowni. Jeśli fryzjer obcina włosy,
// klient zostaje w poczekalni. Jeśli w poczekalni znajduje się wolne krzesło,
// klient siedzi w nim i czeka na swoją kolej. Jeśli nie ma wolnego krzesła,
// klient odchodzi.

const (
	nudzi_sie = iota
	czekanie
	strzyzenie
)

var logStanu = map[int]string{
	0: "Nudzi się",
	1: "Czekających",
	2: "Obcina",
}
var wg *sync.WaitGroup // Liczba potencjalnych klientów
type Fryzjer struct {
	nazwa string
	sync.Mutex
	stan   int // Nudzi się/Odpowiada/Obcina
	klient *Klient
}

type Klient struct {
	nazwa string
}

func (c *Klient) String() string {
	return fmt.Sprintf("%p", c)[7:]
}

func NowyFryzjer() (b *Fryzjer) {
	return &Fryzjer{
		nazwa: "Andrzej",
		stan:  nudzi_sie,
	}
}

// Fryzjer goroutine
// Checks for customers
// Sleeps - wait for sprawdzajacy to wake him up
func fryzjer(b *Fryzjer, czekajacy chan *Klient, sprawdzajacy chan *Klient) {
	for {
		b.Lock()
		defer b.Unlock()
		b.stan = czekanie
		b.klient = nil

		// czekanie w poczekalni
		fmt.Printf("Czekających w poczekalni: %d\n", len(czekajacy))
		time.Sleep(time.Second * 1)
		select {
		case c := <-czekajacy:
			ObcinanieWlosow(c, b)
			b.Unlock()
		default: // Poczekalnia jest pusta
			fmt.Printf("Fryzjer nudzi się - klient: %s\n", b.klient)
			b.stan = nudzi_sie
			b.klient = nil
			b.Unlock()
			c := <-sprawdzajacy
			b.Lock()
			fmt.Printf("Zapytany przez %s\n", c)
			ObcinanieWlosow(c, b)
			b.Unlock()
		}
	}
}

func ObcinanieWlosow(c *Klient, b *Fryzjer) {
	b.stan = strzyzenie
	b.klient = c
	b.Unlock()
	fmt.Printf("Strzyżenie klienta %s\n", c)
	time.Sleep(time.Second * 15)
	b.Lock()
	wg.Done()
	b.klient = nil
}

// klient goroutine
// just fizzles out if it's full, otherwise the klient
// is passed along to the channel handling it's haircut etc
func klient(c *Klient, b *Fryzjer, czekajacy chan<- *Klient, sprawdzajacy chan<- *Klient) {
	// arrive
	time.Sleep(time.Millisecond * 50)
	// Check on fryzjer
	b.Lock()
	fmt.Printf("Klient %s czeka; fryzjer: %s  | oczekujących: %d, pytający %d - klient: %s\n",
		c, logStanu[b.stan], len(czekajacy), len(sprawdzajacy), b.klient)
	switch b.stan {
	case nudzi_sie:
		select {
		case sprawdzajacy <- c:
		default:
			select {
			case czekajacy <- c:
			default:
				wg.Done()
			}
		}
	case strzyzenie:
		select {
		case czekajacy <- c:
		default: // Full waiting room, leave shop
			wg.Done()
		}
	case czekanie:
		panic("Klient shouldn't check for the Fryzjer when Fryzjer is Czekających the waiting room")
	}
	b.Unlock()
}

func main() {
	b := NowyFryzjer()
	b.nazwa = "Tomek"
	Poczekalnia := make(chan *Klient, 5) // 5 chairs
	Pytajacy := make(chan *Klient, 1)    // Only one waker at a time
	go fryzjer(b, Poczekalnia, Pytajacy)

	time.Sleep(time.Millisecond * 100)
	wg = new(sync.WaitGroup)
	n := 10
	wg.Add(10)

	// Spawn customers
	for i := 0; i < n; i++ {
		time.Sleep(time.Millisecond * 50)
		c := new(Klient)
		go klient(c, b, Poczekalnia, Pytajacy)
	}

	wg.Wait()
	fmt.Println("Żadnych klientów więcej tego dnia")
}
