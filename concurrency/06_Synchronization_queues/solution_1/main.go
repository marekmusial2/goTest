package main

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/marek2222/goTest/concurrency/06_Synchronization_queues/solution_1/queue"
)

func test() {
	work()
}
func code() {
	work()
}
func work() {
	// Sleep up to 10 seconds.
	time.Sleep(time.Duration(rand.Intn(10000)) * time.Millisecond)
}
func pingPong() {
	// Sleep up to 2 seconds.
	time.Sleep(time.Duration(rand.Intn(2000)) * time.Millisecond)
}
func tester(q *queue.Queue) {
	for {
		test()
		q.StartT()
		fmt.Println("Tester starts")
		pingPong()
		fmt.Println("Tester ends")
		q.EndT()
	}
}
func programmer(q *queue.Queue) {
	for {
		code()
		q.StartP()
		fmt.Println("Programmer starts")
		pingPong()
		fmt.Println("Programmer ends")
		q.EndP()
	}
}
func main() {
	q := queue.New()
	for i := 0; i < 10; i++ {
		go programmer(q)
	}
	for i := 0; i < 5; i++ {
		go tester(q)
	}
	select {}
}
